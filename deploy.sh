rm -rf dist/*

cat mc.js \
| wc -l \
| \xargs -I%lines% bash -c 'echo $((%lines%-2))' \
| xargs -I%lines% head -n %lines% mc.js > dist/mc.gs

cat dist/mc.gs | pbcopy
