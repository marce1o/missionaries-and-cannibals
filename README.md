# Missionaries And Cannibals

From [Wikipedia](https://en.m.wikipedia.org/wiki/Missionaries_and_cannibals_problem):
> In the missionaries and cannibals problem, three missionaries and three cannibals must cross a river using a boat which can carry at most two people, under the constraint that, for both banks, if there are missionaries present on the bank, they cannot be outnumbered by cannibals (if they were, the cannibals would eat the missionaries). The boat cannot cross the river by itself with no people on board.

```javascript
var MissionariesAndCannibals = require('./mcApp.js');

game = new MissionariesAndCannibals();
game.move(game.M1);
```

## Backlog

### Pre Alpha
- ✅ First playable version available to play test

### Alpha
- ✅ Feedback from play test
    - ✅ Tiny message adjustments

### 1.0
- ✅ Incorporate in the game (previously on Player Bot) the logic to handle:
    - ✅ `restart`, `help`, `about` commands
    - ✅ `move` command, including the dictionary function
- ✅ Remove the feedback function - link added to “about” message
- ✅ Update about, help and intro messages according to the latest changes
- ✅ Refactor constructor and restart functions to comply with Player Bot contract
- ✅ Separate NodeJS module to a different file

### 2.0
- Implement double move command
