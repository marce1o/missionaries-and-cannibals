// https://github.com/substack/tape
var test = require('tape-catch');
var MissionariesAndCannibals = require('./mcApp.js');

const STATE_TO_PRINT_MAP = [{
  state: '{"m1":"left","m2":"left","m3":"left","c1":"left","c2":"left","c3":"left","boat":"left"}',
  print: "`-m1-m2-m3-c1-c2-c3-\\_/.....-----`"
}, {
  state: '{"m1":"left","m2":"left","m3":"left","c1":"left","c2":"left","c3":"right","boat":"right"}',
  print: "`-m1-m2-m3-c1-c2-.....\\_/---c3---`"
}, {
  state: '{"m1":"left","m2":"left","m3":"left","c1":"left","c2":"right","c3":"right","boat":"left"}',
  print: "`-m1-m2-m3-c1-\\_/.......--c2-c3--`"
}, {
  state: '{"m1":"left","m2":"left","m3":"left","c1":"right","c2":"right","c3":"right","boat":"right"}',
  print: "`--m1-m2-m3--.....\\_/--c1-c2-c3--`"
}, {
  state: '{"m1":"left","m2":"left","m3":"right","c1":"right","c2":"right","c3":"right","boat":"left"}',
  print: "`--m1-m2--\\_/.......-m3-c1-c2-c3-`"
}, {
  state: '{"m1":"left","m2":"right","m3":"right","c1":"right","c2":"right","c3":"right","boat":"right"}',
  print: "`---m1---.....\\_/-m2-m3-c1-c2-c3-`"
}, {
  state: '{"m1":"right","m2":"right","m3":"right","c1":"right","c2":"right","c3":"right","boat":"left"}',
  print: "`-----\\_/.....-m1-m2-m3-c1-c2-c3-`"
}, {
  state: '{"m1":"left","m2":"left","m3":"left","c1":"boat","c2":"left","c3":"left","boat":"left"}',
  print: "`-m1-m2-m3-c2-c3-\\c1/.......-----`"
}, {
  state: '{"m1":"left","m2":"left","m3":"boat","c1":"right","c2":"boat","c3":"left","boat":"right"}',
  print: "`--m1-m2-c3--.....\\m3_c2/---c1---`"
}, {
  state: '{"m1":"left","m2":"left","m3":"boat","c1":"boat","c2":"left","c3":"left","boat":"left"}',
  print: "`-m1-m2-c2-c3-\\m3_c1/.......-----`"
}]

test('Initial state', function(t) {
  game = new MissionariesAndCannibals();

  t.equal(game.state(), JSON.stringify(_createState(game.LEFT, game.LEFT, game.LEFT, game.LEFT, game.LEFT, game.LEFT, game.LEFT)));
  t.end();
});

test('Moving a missionary to the boat', function(t) {
  game = new MissionariesAndCannibals();

  message = game.move(game.M1);
  t.equal(message, 'm1 moved from left to boat.');
  t.equal(game.state(), JSON.stringify(_createState(game.BOAT, game.LEFT, game.LEFT, game.LEFT, game.LEFT, game.LEFT, game.LEFT)));
  t.end();
});

test('Moving without specifying what', function(t) {
  game = new MissionariesAndCannibals();

  expectedState = game.state();
  t.equal(game.move(), 'You need to define who or what will move.');
  t.equal(game.state(), expectedState);
  t.end();
});

test('Moving an unknown character or object', function(t) {
  game = new MissionariesAndCannibals();

  expectedState = game.state();
  t.equal(game.move('m4'), 'You either can move a cannibal, a missionary or the boat. At least in the game.');
  t.equal(game.state(), expectedState);
  t.end();
});

test('Moving an empty boat from left to right', function(t) {
  game = new MissionariesAndCannibals();

  expectedState = game.state();
  t.equal(game.move(game.BOAT), 'Er... the boat cannot move by itself. Move someone to the boat first.');
  t.equal(game.state(), expectedState);
  t.end();
});

test('Moving a cannibal when boat is off', function(t) {
  game = new MissionariesAndCannibals();

  game.move(game.C1);
  game.move(game.BOAT);
  expectedState = game.state();
  t.equal(game.move(game.M2), 'There is no walking on the water. Maybe you need to move the boat.');
  t.equal(game.state(), expectedState);
  t.end();
});

test('Moving the boat with a cannibal', function(t) {
  game = new MissionariesAndCannibals();

  message = game.move(game.C1);
  t.equal(message, 'c1 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  t.equal(game.state(), JSON.stringify(_createState(game.LEFT, game.LEFT, game.LEFT, game.BOAT, game.LEFT, game.LEFT, game.RIGHT)));
  t.end();
});

test('Moving a cannibal when the boat is full', function(t) {
  game = new MissionariesAndCannibals();

  game.move(game.M1);
  game.move(game.M2);
  expectedState = game.state();
  t.equal(game.move(game.C1), 'Sorry, but the boat is full. Time to depart?');
  t.equal(game.state(), expectedState);
  t.end();
});

test('Cannibal crossing a missionary to the other side', function(t) {
  game = new MissionariesAndCannibals();
  message = game.move(game.C1);
  t.equal(message, 'c1 moved from left to boat.');
  message = game.move(game.M3);
  t.equal(message, 'm3 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  message = game.move(game.M3);
  t.equal(message, 'm3 moved from boat to right.');
  t.equal(game.state(), JSON.stringify(_createState(game.LEFT, game.LEFT, game.RIGHT, game.BOAT, game.LEFT, game.LEFT, game.RIGHT)));
  t.end();
});

test('About message', function(t) {
  game = new MissionariesAndCannibals();
  message = game.about();
  t.equal(message, '*Missionaries and Cannibals 1.0*\nby <https://bitbucket.org/marce1o/|Marcelo Augusto>\n\n_What\'s new:_\n- First stable version. \n\nType *help* for more information\nPlease send your feedback <https://bitbucket.org/marce1o/missionaries-and-cannibals/issues|here>');
  t.end();
});

test('Move after game has ended', function(t) {
  game = new MissionariesAndCannibals();
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from left to boat.');
  t.false(game.isGameOver(), 'The game should not be over.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.\n\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.');
  t.true(game.isGameOver(), 'The game should be over.');
  message = game.move(game.BOAT);
  t.equal(message, 'No more moves: the game is over. You need to restart it.');
  t.end();
});

test('Restart and play an ended fame', function(t) {
  game = new MissionariesAndCannibals();
  initialState = game.state();
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from left to boat.');
  t.false(game.isGameOver(), 'The game should not be over.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.\n\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.');
  t.true(game.isGameOver(), 'The game should be over.');
  game.restart();
  t.equals(game.state(), initialState);
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from left to boat.');
  t.end();
});

// https://en.wikipedia.org/wiki/Missionaries_and_cannibals_problem#Solution
test('Win the game with the shortest path', function(t) {
  game = new MissionariesAndCannibals();
  //2
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from left to boat.');
  message = game.move(game.C1);
  t.equal(message, 'c1 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  //3
  message = game.move(game.C1);
  t.equal(message, 'c1 moved from boat to right.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from right to left.');
  //4
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from boat to left.');
  message = game.move(game.C2);
  t.equal(message, 'c2 moved from left to boat.');
  message = game.move(game.C3);
  t.equal(message, 'c3 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  //5
  message = game.move(game.C2);
  t.equal(message, 'c2 moved from boat to right.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from right to left.');
  //6
  message = game.move(game.C3);
  t.equal(message, 'c3 moved from boat to left.');
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from left to boat.');
  message = game.move(game.M2);
  t.equal(message, 'm2 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  //7
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from boat to right.');
  message = game.move(game.C2);
  t.equal(message, 'c2 moved from right to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from right to left.');
  //8
  message = game.move(game.C2);
  t.equal(message, 'c2 moved from boat to left.');
  message = game.move(game.M3);
  t.equal(message, 'm3 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  //9
  message = game.move(game.M2);
  t.equal(message, 'm2 moved from boat to right.');
  message = game.move(game.M3);
  t.equal(message, 'm3 moved from boat to right.');
  message = game.move(game.C1);
  t.equal(message, 'c1 moved from right to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from right to left.');
  //10
  message = game.move(game.C2);
  t.equal(message, 'c2 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  //11
  message = game.move(game.C1);
  t.equal(message, 'c1 moved from boat to right.');
  message = game.move(game.C2);
  t.equal(message, 'c2 moved from boat to right.');
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from right to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from right to left.');
  //12
  message = game.move(game.C3);
  t.equal(message, 'c3 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  //13
  message = game.move(game.C3);
  t.equal(message, 'c3 moved from boat to right.');
  message = game.move(game.M1);
  t.equal(message, 'm1 moved from boat to right.\n\nAnd now all the missionaries and cannibals are on the *right* side of the river. Got the pun? *WELL DONE!*');
  t.true(game.isGameOver(), 'The game should be over.');
  t.equals(game.state(), JSON.stringify(_createState(game.RIGHT, game.RIGHT, game.RIGHT, game.RIGHT, game.RIGHT, game.RIGHT, game.RIGHT)));

  t.end();
});

test('Player lose the game', function(t) {
  game = new MissionariesAndCannibals();

  message = game.move(game.M1);
  t.equal(message, 'm1 moved from left to boat.');
  message = game.move(game.C1);
  t.equal(message, 'c1 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.');
  //3
  message = game.move(game.C1);
  t.equal(message, 'c1 moved from boat to right.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from right to left.');
  //4
  message = game.move(game.C2);
  t.equal(message, 'c2 moved from left to boat.');
  message = game.move(game.BOAT);
  t.equal(message, 'Boat moved from left to right.\n\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.');
  t.true(game.isGameOver(), 'The game should be over.');
  t.equals(game.state(), JSON.stringify(_createState(game.BOAT, game.LEFT, game.LEFT, game.RIGHT, game.BOAT, game.LEFT, game.RIGHT)));

  t.end();
});

test('Help text', function(t) {
  game = new MissionariesAndCannibals();
  t.equal(game.help(), 'To move the first missionary, type *move m1* (or simply *m1*).\nTo move the second cannibal, type *move c2* (or *c2*).\nTo move the boat, type *move boat* (or *b*).\nYou can also *restart* the game at any time.');
  t.end();
});

test('Intro text', function(t) {
  game = new MissionariesAndCannibals();
  t.equal(game.intro(), 'In the Missionaries and Cannibals game, three missionaries and three cannibals must cross a river using a boat which can carry at most two people, under the constraint that, for both banks, if there are missionaries present on the bank, they cannot be outnumbered by cannibals (if they were, the cannibals would eat the missionaries). The boat cannot cross the river by itself with no people on board.' + '\n\n' + 'To move the first missionary, type *move m1* (or simply *m1*).\nTo move the second cannibal, type *move c2* (or *c2*).\nTo move the boat, type *move boat* (or *b*).\nYou can also *restart* the game at any time.' + '\n\nLet\'s start!\n\n' + "`-m1-m2-m3-c1-c2-c3-\\_/.....-----`");
  t.end();
});

test('Print initial state', function(t) {

  var game = new MissionariesAndCannibals();
  for (item of STATE_TO_PRINT_MAP) {
    game.restart(item.state);
    t.equal(game.print(), item.print);
  }
  t.end();
});


function _createState(
  m1,
  m2,
  m3,
  c1,
  c2,
  c3,
  boat) {
  return {
    "m1": m1,
    "m2": m2,
    "m3": m3,
    "c1": c1,
    "c2": c2,
    "c3": c3,
    "boat": boat
  }
}
