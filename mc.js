//
// Create an instance of the game.
//
//
var MissionariesAndCannibals = (function(inititalState) {
  function MissionariesAndCannibals(inititalState) {

    // Constants
    this.LEFT = 'left';
    this.RIGHT = 'right';
    this.BOAT = 'boat';
    this.M1 = 'm1';
    this.M2 = 'm2';
    this.M3 = 'm3';
    this.C1 = 'c1';
    this.C2 = 'c2';
    this.C3 = 'c3';

    this.RESTART = /^restart$/;
    this.HELP = /^help$|^\?$|^h$/;
    this.ABOUT = /^about$/;
    this.MOVE = /^(move|mv)\s*([mc][1-3]|b(oat){0,1}|cannibal\s*[1-3]|missionary\s*[1-3])$/;
    this.SHORT_MOVE = /^((m[1-3]|c[1-3]|b(oat){0,1}))$/;

    // Initialize game state and status
    this.restart = function(initialState) {
      // console.log('Starting game...');
      gameOver = false;
      if (initialState) {
        state = JSON.parse(initialState);
      } else {
        state = {}
        state[this.M1] = this.LEFT;
        state[this.M2] = this.LEFT;
        state[this.M3] = this.LEFT;
        state[this.C1] = this.LEFT;
        state[this.C2] = this.LEFT;
        state[this.C3] = this.LEFT;
        state[this.BOAT] = this.LEFT;
      }
    };

    this.state = function() {
      return JSON.stringify(state);
    };

    //TODO: This should be part of the MC Bot wrapper, not the MC itself
    this.execute = function(input) {

      if (input.match(this.HELP)){
        return this.help();
      }

      if (input.match(this.ABOUT)){
        return this.about();
      }

      if (input.match(this.RESTART)){
        this.restart();
        return 'OK. Sometimes all we need is a fresh start:\n\n' + game.print();
      }

      match = input.match(this.MOVE) || input.match(this.SHORT_MOVE);

      if (match) {
        return this.move(this.dictionary(match[2])) + "\n\n" + this.print();
      } else {
        return 'I did not get that. You can type *help* or *?* to know what I can do.';
      }

    };

    // Move an entity in the game (character or boat)
    this.move = function(entity) {
      // console.log('Trying to move', entity);
      if (gameOver) {
        return 'No more moves: the game is over. You need to restart it.';
      }

      if (!entity) {
        return 'You need to define who or what will move.';
      }

      var response = undefined;

      // Collecting some information from the current state
      from = undefined; // Where the entity to be moved is
      boarded = 0; // how many occupants on the boat
      to = undefined; // where the entity can be moved to

      for (var attribute in state) {
        if (state.hasOwnProperty(attribute)) {
          if (attribute == entity) {
            from = state[attribute];
          }
          if (state[attribute] == this.BOAT) {
            boarded++;
          }
          if (state[attribute] == this.BOAT) {
            found = true;
          }
        }
      }

      if (entity == this.BOAT) { // First let's handle a boat move...

        // Checking whether the boat is empty
        if (boarded == 0) {
          return 'Er... the boat cannot move by itself. Move someone to the boat first.';
        }
        // Finally move the boat
        if (state[this.BOAT] == this.LEFT) {
          state[this.BOAT] = this.RIGHT;
          response = 'Boat moved from left to right.';
        } else {
          state[this.BOAT] = this.LEFT;
          response = 'Boat moved from right to left.';
        }

      } else { // and then let's handle a character move

        // Confirming it is an existent character
        if (!from) {
          return 'You either can move a cannibal, a missionary or the boat. At least in the game.';
        }

        // If the character is in the boat...
        if (from == this.BOAT) {
          // ... the character must move to the bank where the boat is at
          to = state[this.BOAT];
          state[entity] = to;
        } else {
          // Otherwise the destination is the boat:
          // (a) is the boat on the same bank?
          if (from != state[this.BOAT]) {
            return 'There is no walking on the water. Maybe you need to move the boat.';
          }
          // (b) is there room?
          if (boarded >= 2) {
            return 'Sorry, but the boat is full. Time to depart?'
          }
          // Come aboard!
          to = this.BOAT
          state[entity] = to;
        }

        response = entity + ' moved from ' + from + ' to ' + to + '.';
      }
      // console.log(response);
      check = this._checkGameEnd();

      if (check) {
        response += '\n' + check;
      }

      return response;
    };

    this._checkGameEnd = function() {
      var message = '';
      // Let's do some math
      var counters = {
        left: {
          c: 0,
          m: 0
        },
        right: {
          c: 0,
          m: 0
        },
        boat: []
      }
      for (var attribute in state) {
        if (state.hasOwnProperty(attribute)) {
          if (attribute != this.BOAT) {
            var character = attribute[0];
            if ((state[attribute] == this.BOAT)) {
              counters[state[this.BOAT]][character]++;
              counters.boat.push(attribute);
            } else {
              counters[state[attribute]][character]++;
            }
          }
        }
      }
      // If everybody is on the right side and the boat is empty, the player wins!
      if (counters.right.c + counters.right.m == 6 && counters.boat.length == 0) {
        gameOver = true;
        message = '\nAnd now all the missionaries and cannibals are on the *right* side of the river. Got the pun? *WELL DONE!*';
      }
      // Let's check if a missionary is in danger
      if ((counters.left.c > counters.left.m && counters.left.m > 0) || (counters.right.c > counters.right.m && counters.right.m > 0)) {
        gameOver = true;
        message = '\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.';
      }
      // console.log(message);
      return message;
    };

    this.print = function() {

      var organizer = {
        left: [],
        right: [],
        boat: []
      }

      for (var entity in state) {
        if (state.hasOwnProperty(entity)) {
          if (entity != this.BOAT) {
            var location = state[entity];
            organizer[location].push(entity);
          }
        }
      }

      var boat = organizer.boat.length > 0 ? organizer.boat.join('_') : '_';
      boat = '\\' + boat + '/';

      var left = organizer.left.length > 0 ? organizer.left.join('-') : '-';
      var right = organizer.right.length > 0 ? organizer.right.join('-') : '-';

      const PADDING = [
        '--',
        '---',
        '--',
        '--',
        '-',
        '-',
        '-'
      ];

      const RIVER = {5: '.....', 7: '.......'}

      left = PADDING[organizer.left.length] + left + PADDING[organizer.left.length];
      right = PADDING[organizer.right.length] + right + PADDING[organizer.right.length];

      var water = RIVER[34 - (left.length + boat.length + right.length + 2)];
      if (state[this.BOAT] == this.LEFT) {
        boat = boat + water;
      } else {
        boat = water + boat;
      }

      return '`' + left + boat + right + '`';
    }

    this.isGameOver = function() {
      return gameOver;
    }

    this.about = function() {
      return '*Missionaries and Cannibals 1.0*\nby <https://bitbucket.org/marce1o/|Marcelo Augusto>\n\n_What\'s new:_\n- First stable version. \n\nType *help* for more information\nPlease send your feedback <https://bitbucket.org/marce1o/missionaries-and-cannibals/issues|here>'
    }

    this.help = function() {
      return 'To move the first missionary, type *move m1* (or simply *m1*).\nTo move the second cannibal, type *move c2* (or *c2*).\nTo move the boat, type *move boat* (or *b*).\nYou can also *restart* the game at any time.';
    }

    this.intro = function() {
      introMessage = 'In the Missionaries and Cannibals game, three missionaries and three cannibals must cross a river using a boat which can carry at most two people, under the constraint that, for both banks, if there are missionaries present on the bank, they cannot be outnumbered by cannibals (if they were, the cannibals would eat the missionaries). The boat cannot cross the river by itself with no people on board.';
      return introMessage + '\n\n' + this.help() + '\n\nLet\'s start!\n\n' + this.print();
    }

    this.dictionary = function(term) {
      data = {
        m1: "m1",
        missionary1: "m1",
        "missionary 1": "m1",
        m2: "m2",
        missionary2: "m2",
        "missionary 2": "m2",
        m3: "m3",
        missionary3: "m3",
        "missionary 3": "m3",
        c1: "c1",
        cannibal1: "c1",
        "cannibal 1": "c1",
        c2: "c2",
        cannibal2: "c2",
        "cannibal 2": "c2",
        c3: "c3",
        cannibal3: "c3",
        "cannibal 3": "c3",
        boat: "boat",
        b: "boat"
      };
      return data[term.toLowerCase()];
    }

    this.restart(inititalState);

  }
  return MissionariesAndCannibals;
}());
